const express = require('express');
const peopleRouter = express.Router();



peopleRouter.route('/')
	.get(function(req,res) {
		res.send('Hello there People');
	});

peopleRouter.route('/single')
	.get((req,res) => {
		res.send('Hello Single People');
	});


module.exports = peopleRouter;
