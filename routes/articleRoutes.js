const express = require('express');
const articleRouter = express.Router();



articleRouter.route('/')
	.get(function(req,res) {
		res.send('Hello there Articles');
	});

articleRouter.route('/single')
	.get((req,res) => {
		res.send('Hello Single Article');
	});


module.exports = articleRouter;
