const express = require('express');
const adminRouter = express.Router();
const mongodb = require('mongodb');
const mongoClient = mongodb.MongoClient;
const propertiesReader = require('properties-reader');

const app = express();

var properties = propertiesReader('db.txt');

//abstractions
const theAbstraction = properties.get('dbAbstract');
const dbAbstract = require(path.join(__dirname, 'db', theAbstraction));

var g_title = properties.get('title');

var readPeople = function(callback) {
	const url = 'mongodb://localhost:27017';
	const dbName = 'firstExpress';
	var people = null;

	mongoClient.connect(url, function (err, client) {
		var db = client.db(dbName);
		db.collection('people', function (err, col) {
			col.find().toArray(function (err, rslt) {
				people = rslt;
				client.close();

				callback(people);
			})
//				.then()
		});
	});
}


adminRouter.route('/')
	.get(function(req,res) {
		dbAbstract.readPeople(function(people) {
			res.render('admin', {people: people, title: g_title});
		});
	});

adminRouter.route('/newPeople')
	.get((req,res) => {
		res.render('adminNew', {title: g_title});
	});

adminRouter.route('/newPeople')
	.post((req,res) => {
			var person = new Object();
			person.name = req.body.name;
			person.pic = req.body.pic;
			person.description = req.body.description;
			person.url = req.body.url;

			addPerson(person, function(result) {
				res.redirect('/admin');
			});
	});

adminRouter.route('/editPeople')
	.post((req,res) => {
		var actionType = req.body.actionType;
		var id = req.body._id;
		var pointInEdit = req.body.pointInEdit;

		if (actionType == 'delete')
		{
			dbAbstract.deletePerson(id, function(result) {
				res.redirect('/admin');
			});
		} else if(actionType == 'edit') 
		{
			if (pointInEdit == 'prompt')
			{
				var person = new Object();
				person._id = id;
				person.name = req.body.name;
				person.pic = req.body.pic;
				person.description = req.body.description;
				person.url = req.body.url;

				res.render('adminEdit', {person: person, title: g_title});				
			} else if (pointInEdit == 'execute')
			{
				var person = new Object();
				person._id = id;
				person.name = req.body.name;
				person.pic = req.body.pic;
				person.description = req.body.description;
				person.url = req.body.url;

				dbAbstract.editPerson(person, function(result) {
					res.redirect('/admin');
				});
			} else
				res.redirect('/admin');
		} else
				res.redirect('/admin');
		

	});

module.exports = adminRouter;
