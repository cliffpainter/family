/*
 * File: app.js
 * 
 * This source file is a study in using XJS templating to enable Node
 * and Express to generate dynamic content.  HTML inserted with in-place
 * javascript makes this process simple and modular.  This version
 * extends the previous by adding routes.
 * 
 * This is an extension of the source file nextExpress.js by using
 * a MongoDB database to hold "people" information instead of storing
 * in a properties.
 */


// Pull in required modules
const express = require('express');
const path = require('path');
const mongoClient = require('mongodb').MongoClient;
const fs = require('fs');
const propertiesReader = require('properties-reader');

//Instance core scripting tools
var app = express();
var properties = propertiesReader('db.txt');

//abstractions
const theAbstraction = properties.get('dbAbstract');
const dbAbstract = require(path.join(__dirname, 'db', theAbstraction));

const bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// create routes
const articleRouter = require(path.join(__dirname, 'routes', 'articleRoutes'));
const adminRouter = require(path.join(__dirname, 'routes', 'adminRoutes'));
const peopleRouter = require(path.join(__dirname, 'routes', 'peopleRoutes'));

// Set up routes/page navigation
app.set('views', 'views');
app.set('view engine', 'ejs');
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/articles', articleRouter);
app.use('/admin', adminRouter);
app.use('/people', peopleRouter);

/*
Start of functionality
*/

console.log();
console.log("-------- Running nextExpressDb.js --------");

var g_title = properties.get('title');

/*
 * Function get
 * This function provides a rendering for the default route (/)
 */
app.get('/', function(req, res) {
	dbAbstract.readPeople(function(people) {
		res.render('index', {people: people, title: g_title});
	});
})

// Now that everything is set up, crank up the server
app.listen(3000, function() {
	console.log("Listening on port 3000..");
})
