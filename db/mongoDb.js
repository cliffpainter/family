const mongoClient = require('mongodb').MongoClient;


/*
 * Function readPeople
 * This function opens a connection to the Mongo database,
 * queries it for the 'people' collection, and returns the
 * results formatted as a json array via the callback
 */
const readPeople = function(callback) {
	const url = 'mongodb://localhost:27017';
	const dbName = 'firstExpress';
	var people = null;

	mongoClient.connect(url, function (err, client) {
		var db = client.db(dbName);
		db.collection('people', function (err, col) {
			col.find().toArray(function (err, rslt) {
				people = rslt;
				client.close();

				callback(people);
			})
//				.then()
		});
	});
}

function deletePerson(id, callback)
{
	const url = 'mongodb://localhost:27017';
	const dbName = 'firstExpress';

	mongoClient.connect(url, function (err, client) {
		var db = client.db(dbName);
		db.collection('people', function (err, col) {
			col.deleteOne({_id: new mongodb.ObjectID(id)}, function(err, rslt) {
				client.close();

				callback(rslt);
				
			});
		});
	});
}

function editPerson(person, callback)
{
	const url = 'mongodb://localhost:27017';
	const dbName = 'firstExpress';

	mongoClient.connect(url, function (err, client) {
		var db = client.db(dbName);
		db.collection('people', function (err, col) {
			col.findOneAndUpdate({_id: new mongodb.ObjectID(person._id)},
				{'$set': {'name': person.name, 'pic': person.pic, 'url': person.url, 'description': person.description }},
				function(err, rslt) {
					client.close();
					callback(rslt);
				}
			);
		});
	});
}

function addPerson(person, callback)
{
	const url = 'mongodb://localhost:27017';
	const dbName = 'firstExpress';

	mongoClient.connect(url, function (err, client) {
		var db = client.db(dbName);
		db.collection('people', function (err, col) {
			col.insertOne(
				{'name': person.name, 'pic': person.pic, 'url': person.url, 'description': person.description },
				function(err, rslt) {
					client.close();
					callback(rslt);
				}
			);
		});
	});
}



exports.readPeople = readPeople;
exports.deletePerson = deletePerson;
exports.editPerson = editPerson;
exports.addPerson = addPerson;
