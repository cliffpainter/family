var express = require('express');
var path = require('path');
var propertiesReader = require('properties-reader');

var properties = propertiesReader('db.txt');

/*
 * Function readPeople
 * This function reads the properties database,
 * queries it for the 'people' collection, and returns the
 * results formatted as a json array via the callback
 */
const readPeople = function(callback) {
	var i = 0;
	var people = new Array();

	var name = properties.get('person.name.' + i);
	while (name)
	{
		var pic = properties.get('person.pic.' + i);
		var description = properties.get('person.description.' + i);
		var url = properties.get('person.url.' + i);

		var person = new Object();
		person.name = name;
		person.pic = pic;
		person.description = description;
		person.url = url;
		people.push(person);
		name = properties.get('person.name.' + ++i);
	}

	callback(people);
}

function deletePerson(callback)
{
	console.log("Note that with this db abstration, delete/edit/add are no-op'd");
}

function editPerson(callback)
{
	console.log("Note that with this db abstration, delete/edit/add are no-op'd");
}

function addPerson(callback)
{
	console.log("Note that with this db abstration, delete/edit/add are no-op'd");
}


exports.readPeople = readPeople;
exports.deletePerson = deletePerson;
exports.editPerson = editPerson;
exports.addPerson = addPerson;

