var express = require('express');
var path = require('path');
var fs = require('fs');
var propertiesReader = require('properties-reader');

var app = express();
var properties = propertiesReader('db.txt');

app.set('views', 'views');
app.set('view engine', 'ejs');

app.use('/public', express.static(path.join(__dirname, 'public')));

console.log("Here we are");

var g_title = properties.get('title');
var g_people = new Array();

{
	var i = 0;
	var name = properties.get('person.name.' + i);
	while (name)
	{
		var pic = properties.get('person.pic.' + i);
		var description = properties.get('person.description.' + i);
		var url = properties.get('person.url.' + i);

		var person = new Object();
		person.name = name;
		person.pic = pic;
		person.description = description;
		person.url = url;
		g_people.push(person);
		name = properties.get('person.name.' + ++i);
	}
}


app.get('/', function(req, res) {
	res.render('index', {people: g_people, title: g_title});
})

app.listen(3000, function() {
	console.log("Listening on port 3000..");
})