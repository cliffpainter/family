name=Cole Sillyhead
pet=Rhino

title=The Family

dbAbstract=propertiesDb

person.name.0=Cliff
person.pic.0=public/img/cliff.jpg
person.description.0=Head honcho of the Painters
person.url.0=http://www.google.com

person.name.1=Cole
person.pic.1=public/img/cole.jpg
person.description.1=Wacky Fuzzy head of the Painters
person.url.1=https://www.youtube.com/watch?v=oHg5SJYRHA0

person.name.2=Cass
person.pic.2=public/img/cass.jpg
person.description.2=Evil genius of the Painters
person.url.2=https://www.taylorswift.com

